/*
                                _____
                               |     |
 Hardware:             +5V     |     |
             ________   |      |     |
      ___   |        |  |      |     |  ___
 +5V-|___|--|RES  VCC|--+   B3-|I4 O4|-|___|-+Q3 brown
            |        |         |     |  ___
        B3--|PB3  PB2|---------|I5 O5|-|___|-+Q2 yellow
                     |         |     |  ___
          --|PB4  PB1|---------|I6 O6|-|___|-+Q4 blue
            |        |         |     |  ___
         |--|GND  PB0|---------|I7 O7|-|___|-+Q1 red
            |________|         |     |
             ATtiny13        |-|GNDCC|------- +12Volt white
                               |_____|    |__ +12Volt white
                                ULN2003
Funktioning: sequence: red-yellow-brown-blue
    Stepmotor drive sequence and encoding:
      Portbit      PB0   PB1   PB2   PB3   |            |
      Color         rd    bl    yl    bn   | Portbit    | Byte
      Step          O7    O6    O5    O4   | 3  2  1  0 |
      -------------------------------------+------------+------
         1          1      0    1     0    | 0  1  0  1 |  05
         2          0      0    1     1    | 1  1  0  0 |  12 / 0C
         3          0      1    0     1    | 1  0  1  0 |  10 / 0A
         4          1      1    0     0    | 0  0  1  1 |  03
If you would use D8-D11 on an Atmega328, you could use the same values for the Portbits,
as D8-D11 is equivalent to PB0-PB3

red    ------------+------------ brown
yellow-------------+------------ blue



*/


#define delayTime  100
#define servoPin 4 //pin for servo
void setup()
{
	DDRB = 31; // set output
	PORTB = 0;
	stepper(3);
	delay(1000);
	for (byte pos = 180; pos > 0; pos--)
	{
		pulseOut(servoPin, pos);
		delay(20);
	}
	stepperCCW(3);
	PORTB = 0;
}
void loop()
{
	/*
	stepper(3);
	delay(1500);
	 for (byte pos=180;pos>0;pos--)
	{
	pulseOut(servoPin,pos);
	delay(20);
	}
	stepperCCW(3);
	delay(1500);
	*/
}
void stepper(byte turn)
{
	for (byte pos = 0; pos < turn; pos++)
	{
		PORTB = 5;
		delay(delayTime);
		PORTB = 12;
		delay(delayTime);
		PORTB = 10;
		delay(delayTime);
		PORTB = 3;
		delay(delayTime);
	}
}

void pulseOut( byte pin, byte p)
{
	digitalWrite(pin, HIGH);
	delayMicroseconds(300 + p * (2500 / 180));
	digitalWrite(pin, LOW);
}
void stepperCCW(byte turn)
{
	for (byte pos = 0; pos < turn; pos++)
	{
		PORTB = 3;
		delay(delayTime);
		PORTB = 10;
		delay(delayTime);
		PORTB = 12;
		delay(delayTime);
		PORTB = 5;
		delay(delayTime);
	}
}
